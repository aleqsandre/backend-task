<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use Laravie\Parser\Xml\Reader;
use Laravie\Parser\Xml\Document;

/*
|--------------------------------------------------------------------------
| Parser Controller
|--------------------------------------------------------------------------
|
| This controller reads the XML file.
| Process the data of the XML file.
| Saves the data in the MySQL database.
|
*/
class Parser extends Controller
{
    /**
     * List of processed products
     *
     * @var array
     */
    protected $finalProducts = [];

    /**
     * Different categories mapping 
     *
     * @var array
     */
    protected $usedTypes = [];


    public function index(){

        //Read the XML.
        $xml = (new Reader(new Document()))->load(storage_path('app/xml/all_products.xml'));

        //Get products array from XML.
        $products = $xml->parse([
            'items' => ['uses' => 'channel.product[name,type,size,color,sku,weight,is_in_stock,qty,price,image1,image2,image3,image4,image5,image6,image7,image8,image9,image10,image11,image12,description]'],
        ]);

        $currentProduct['name'] = '';
        foreach($products["items"] as $key => $value):
            if($currentProduct['name'] != $value['name']):

                //add product to final array
                if($key!=0):
                    $this->addProduct($currentProduct);
                endif;

                $currentProduct = array(
                    "name" => $value['name'],
                    "categories_id" => $this->getCategory($value["type"]),
                    "meta" => [$this->getMetaData($value)],
                    "weight" => $value['weight'],
                    "price" => $value['price'],
                    "images" => $this->getImages($value),
                    "description" => $value['description'],
                );
            else:
                $currentProduct["meta"][] = $this->getMetaData($value);
            endif;
        endforeach;

        //add last product to final array
        $this->addProduct($currentProduct);

        //Insert products into database.
        Product::insert($this->finalProducts);

        return ("Data imported successfully. <br />Inserted ".count($this->finalProducts)." products from ".$key.".");
    }

    /**
     * Combines all product images with indexes.
     *
     * @param  array  $product
     * @return  array
     */
    protected function getImages( array $product){
        $images=[];
        for($i=1; $i<=12; $i++):
            if($product["image".$i]) $images[] = array("p" => $product["image".$i], "i"=> $i);
        endfor;

        return $images;
    }

    /**
     * Combines several parameters for same products.
     *
     * @param  array  $product
     * @return  array
     */
    protected function getMetaData(array $product){
        return [
            "color" => $product["color"],
            "size" => $product["size"],
            "qty" => $product["qty"],
            "sku" => trim($product["sku"], "-".$product["color"]."-".$product["size"])
        ];
    }

    /**
     * Ads product to finalProducts array.
     *
     * @param  array  $product
     */
    protected function addProduct( array $product){
        $product["meta"] = json_encode($product["meta"]);
        $product["images"] = json_encode($product["images"]);

        $this->finalProducts[]=$product;
    }

    /**
     * String processing to process product categories.
     *
     * @param  string  $str
     * @return integer
     */
    protected function getCategory( string $str){
        $allPath = $this->getAllPath($str);

        $id=false;
        foreach($allPath as $value):
            if(!isset($this->usedTypes[$value])):
                $id = $this->addNode($value, $id);
                $this->usedTypes[$value]=$id;
            else:
                $id = $this->usedTypes[$value];
            endif;
        endforeach;
        
        return $id;
    }

    /**
     * Gives all possible category from category string.
     *
     * @param  string  $str
     * @return array
     */
    protected function getAllPath( string $str){
        $categories = explode(" > ",$str);

        $array=[];
        for($i=0; $i<count($categories); $i++ ):
            if($i==0):
                $array[] = $categories[$i];
            else:
                $array[] = $array[$i-1]." > ".$categories[$i];
            endif;
        endfor;

        return $array;
    }

    /**
     * adds node into the database.
     *
     * @param  string  $name
     * @param  integer $id
     * @return integer
     */
    protected function addNode( string $name, int $id){
        $name = $this->getLastCategory($name);
        
        $node = Category::create([
            'name' => $name
        ]);
        
        //When category has parent
        if($id):
            $node->parent_id = $id;
            $node->save();
        endif;
        
        return $node->id;
    }

    /**
     * Takes the last element from category string.
     *
     * @param  string  $name
     * @return string
     */
    protected function getLastCategory(string $name){
        $name = explode(" > ", $name);
        return end($name);
    }
}