## About The Repository

This repository includes a task that parses the XML of products and stores them in a database.
The app is written using PHP Laravel[6.x] framework. 

## The app uses:

- [laravie/parser](https://github.com/laravie/parser). Parser Component is a framework agnostic package that provide a simple way to parse XML to array without having to write a complex logic.
- [lazychaser/laravel-nestedset](https://github.com/lazychaser/laravel-nestedset). Package for working with trees in relational databases.

## Required files 
- backend_task.sql is backup of the generated database.
- You can find database structure into migration folder.

## License

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).
